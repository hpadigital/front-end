import * as React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { AdminDashboard } from './AdminDashboard';

describe('AdminDashboard basic tests', () => {
  it('should display the default message', () => {
    const renderResult: RenderResult = render(
      <AdminDashboard/>,
    );
    expect(renderResult.queryByText('Hello from AdminDashboard!')).toBeTruthy();
  });
});