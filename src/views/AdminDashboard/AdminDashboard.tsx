import React from 'react';
import * as S from './AdminDashboard.style';

export const AdminDashboard: React.FC<{}> = () => (
  <S.Container>
    Hello from AdminDashboard!
  </S.Container>
);
