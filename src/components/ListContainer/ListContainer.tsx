import React from 'react';
import * as S from './ListContainer.style';

export const ListContainer: React.FC<{}> = ({children}) => (
  <S.Container>
    {children}
  </S.Container>
);
