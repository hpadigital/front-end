import * as React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { ListContainer } from './ListContainer';

describe('ListContainer basic tests', () => {
  it('should display the default message', () => {
    const renderResult: RenderResult = render(
      <ListContainer/>,
    );
    expect(renderResult.queryByText('Hello from ListContainer!')).toBeTruthy();
  });
});