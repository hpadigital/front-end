---
to: src/<%= path %>/<%= name %>/<%= name %>.tsx
---
import React from 'react';
import * as S from './<%= name %>.style';

export const <%= name %>: React.FC<{}> = () => (
  <S.Container>
    Hello from <%= name %>!
  </S.Container>
);
