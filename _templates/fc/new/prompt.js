module.exports = [
  {
    type: 'input',
    name: 'name',
    message: 'What name do you want?',
  },
  {
    type: 'input',
    name: 'path',
    message: 'What path do you want?',
  },
];
