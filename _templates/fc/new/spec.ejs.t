---
to: src/<%= path %>/<%= name %>/<%= name %>.test.tsx
---
import * as React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { <%= name %> } from './<%= name %>';

describe('<%= name %> basic tests', () => {
  it('should display the default message', () => {
    const renderResult: RenderResult = render(
      <<%= name %>/>,
    );
    expect(renderResult.queryByText('Hello from <%= name %>!')).toBeTruthy();
  });
});
